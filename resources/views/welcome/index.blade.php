<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
           
        </style>
    </head>
    <body>
        <header>Hello World</header>
        <article>
        	<p>
        		<a href="{{ route('events.show', ['id' => 42]) }}">Laravel Hacking and Coffee</a>
        	</p>
        	<p>
        		<a href="/events">Events</a>
        	</p>
        	<p>
        		{!! link_to_route('events.show', $event->name, ['id' => $event->id]) !!}
        	</p>
        </article>
    </body>
</html>
