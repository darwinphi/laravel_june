<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>App</title>
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
	@yield('content')

	@section('advertisement')
		<p>Score some App swag in our store!</p>
	@show
</body>
</html>