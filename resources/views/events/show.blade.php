<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Events</title>
</head>
<body>
	<p>Hey {{ $name }}! We're looking at event ID #{{ $id }}</p>
	<p>{{ $greetings or 'Good Day!' }} </p>
</body>
</html>