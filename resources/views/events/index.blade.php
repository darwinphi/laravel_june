@extends('layouts.app')

@section('content')

<article>
	<ul>
		{{--
			@foreach ($events as $event)
				<li> {{ $event }} </li>
			@endforeach
		--}}

		{{-- If array is empty --}}
		
			@forelse ($events as $event)
				<li> {{ $event }} </li>
				@if (strpos($event, 'Laravel') !== false)
					(Sweet Framework)
				@endif
			@empty
				<li> No events available </li>
			@endforelse	
		
	</ul>

	<h2>Example using partials</h2>
	<table>
		@foreach ($events as $event)
			@include('partials._row', ['event' => $event])
		@endforeach
	</table>

</article>

@endsection

@section('advertisement')
	@parent
	<p>
		App members always get 10% off at Starbucks!
	</p>
@endsection