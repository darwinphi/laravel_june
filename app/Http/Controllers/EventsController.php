<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EventsController extends Controller
{
		public function index() {
			$events = [
				'Laravel Hacking and Coffee',
				'IoT with Raspberry Pi',
				'Free Vue.js Lessons'
			];
			return view('events.index')->with('events', $events);
		}

    public function show($id) {
    	// Debugging
    	// dd($id);
    	
    	// Using 'with'
    	// return view('events.show')->with('id', $id)->with('name', 'Darwin');
    	// return view('events.show')->with('id', $id)->with('name', 'Darwin')->with('name', 'Darwin');
    	
    	// Using `compact`
    	// $name = 'Darwin';
    	// return view('events.show', compact('name', 'id'));
    	
    	// Using `array` in `with`
    	$data = [
				'name' => 'Darwin',
				'id' => $id
			];
			return view('events.show')->with($data);
    }

    public function category($category, $subcategory = 'all') {
    	dd("Category: {$category} Subcategory: {$subcategory}");
    }
}
